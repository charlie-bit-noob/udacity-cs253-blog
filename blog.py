import os
import webapp2
import jinja2
import json
import re
import random
import string
import hashlib
import logging
import time

from datetime import datetime, timedelta
from google.appengine.ext import db
from google.appengine.api import memcache


template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                               autoescape = True)

class Handler(webapp2.RequestHandler):
    def write(self, *a, **lw):
        self.response.out.write(*a, **lw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))


class Blog(db.Model):
    subject = db.StringProperty(required = True)
    content = db.TextProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)

    def to_dict(self):
        d = {'subject': self.subject,
            'content': self.content,
            'created': self.created.strftime("%c")}

        return d

### caching ###

QUERY_FIN = 0
QUERY_START = 0

FRONT_COUNTER = 1
PERML_COUNTER = 1


def mem_front(update = False):
    time.sleep(.5)
    global QUERY_START

    key = 'top'
    b = memcache.get(key)

    if b is None or update:
        QUERY_START = time.time()

        logging.error("DB QUERY")
        b = db.GqlQuery("SELECT * FROM Blog ORDER BY created LIMIT 10")
        b = list(b)
        memcache.set(key, b)
    
    global QUERY_FIN
    global FRONT_COUNTER

    if FRONT_COUNTER < 1:
        FRONT_COUNTER += 1
        QUERY_FIN = 0
    else:
        QUERY_FIN = int(time.time() - QUERY_START)
    return b

def mem_perm(update = False, post_id = None):
    time.sleep(.5)
    global QUERY_START

    key = str(post_id)
    p = memcache.get(key)
    
    if p is None or update:
        QUERY_START = time.time()

        logging.error("DB QUERY")
        p = db.GqlQuery("SELECT * FROM Blog WHERE __key__ = KEY('Blog', :1)", int(post_id))
        if p:
            p = list(p)
            memcache.set(key, p)
    
    global QUERY_FIN
    global PERML_COUNTER

    if PERML_COUNTER < 1:
        PERML_COUNTER += 1
        QUERY_FIN = 0
    else:
        QUERY_FIN = int(time.time() - QUERY_START)    
    return p


class Blogs(Handler):
    def render_blogentries(self):
        b = mem_front()
        self.render("blogs.html", blog = b, time = QUERY_FIN)

    def get(self):
        get_usr_cookie = self.request.cookies.get('username')

        if get_usr_cookie:
            usr_cookie = check_secure_val(str(get_usr_cookie))
            if not usr_cookie:
                self.redirect('/blog/signup')
                return
        else:
            self.redirect('/blog/signup')
            return

        self.render_blogentries()


class NewPost(Handler):
    def render_newpost(self, subject = "", content = "", error = ""):
        self.render("post.html", error=error, subject = subject, content = content)

    def get(self):
        get_usr_cookie = self.request.cookies.get('username')
        if get_usr_cookie:
            usr_cookie = check_secure_val(str(get_usr_cookie))
            if not usr_cookie:
                self.redirect('/blog/signup')
                return
        else:
            self.redirect('/blog/signup')
            return

        self.render_newpost()

    def post(self):
        subject = self.request.get('subject')
        content = self.request.get('content')

        if subject and content:
            b = Blog(subject = subject, content = content)
            bk = b.put()
            mem_front(True)
            mem_perm(True, bk.id())
            self.redirect('/blog/%d' % bk.id())
        else:
            error = "we need both subject and content!"
            self.render_newpost(subject, content, error)


class Permalink(Handler):
    def get(self, post_id):
        get_usr_cookie = self.request.cookies.get('username')
        
        if get_usr_cookie:
            usr_cookie = check_secure_val(str(get_usr_cookie))
            if not usr_cookie:
                self.redirect('/blog/signup')
                return
        else:
            self.redirect('/blog/signup')
            return

        b = mem_perm(False, post_id)

        if b:
            self.render("permalink.html", blog=b, time=QUERY_FIN)
        else:
            self.response.write("404 - Resource not found")


class FlushMemcache(Handler):
    def get(self):
        memcache.flush_all()

        global QUERY_FIN
        global FRONT_COUNTER
        global PERML_COUNTER

        QUERY_FIN = 0
        FRONT_COUNTER = 0
        PERML_COUNTER = 0

        self.redirect('/blog')


class BlogJson(webapp2.RequestHandler):
    def get(self):
        get_usr_cookie = self.request.cookies.get('username')
        
        if get_usr_cookie:
            usr_cookie = check_secure_val(str(get_usr_cookie))
            if not usr_cookie:
                self.redirect('/blog/signup')
                return
        else:
            self.redirect('/blog/signup')
            return

        b = db.GqlQuery("SELECT * FROM Blog ORDER BY created DESC")
        i = 0
        j = list()
        if b:
            for p in b:
                j.append(p.to_dict())

        j = json.dumps(j)

        self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
        self.response.out.write(j)

class PermalinkJson(webapp2.RequestHandler):
    def get(self, post_id):
        get_usr_cookie = self.request.cookies.get('username')
        if get_usr_cookie:
            usr_cookie = check_secure_val(str(get_usr_cookie))
            if not usr_cookie:
                self.redirect('/blog/signup')
                return
        else:
            self.redirect('/blog/signup')
            return

        b = Blog.get_by_id(int(post_id))
        j = dict()
        if b:
            for p in [b]:
                j = json.dumps(p.to_dict())

        self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
        self.response.out.write(j)

app = webapp2.WSGIApplication([(r'/blog/signup', Signup), 
                                (r'/blog/thanks', ThanksHandler), 
                                (r'/blog/login', Login), 
                                (r'/blog/welcome', Welcome), 
                                (r'/blog/logout', Logout), 
                                (r'/blog', Blogs),
                                (r'/blog/flush', FlushMemcache),  
                                (r'/blog/.json', BlogJson), 
                                (r'/blog/newpost', NewPost), 
                                (r'/blog/(\d+)', Permalink),
                                (r'/blog/(\d+).json', PermalinkJson)
                                ], debug=True)
